package com.mkyong.rest;

public class Movie {

	public Movie() {
		super();
	}

	public Movie(Long id, String name, String director, int year) {
		super();
		this.id = id;
		this.name = name;
		this.director = director;
		this.year = year;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", name=" + name + ", director=" + director + ", year=" + year
				+ "]";
	}

	private Long id;
	private String name;
	private String director;
	private int year;

	public Movie(long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

}