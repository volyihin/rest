package com.mkyong.rest;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.common.collect.Lists;

@Path("/movie")
@Produces(MediaType.APPLICATION_JSON)
public class MovieService {

	private static List<Movie> movies = Lists.newArrayList();
	private static final Logger logger = Logger.getLogger(MovieService.class);
	
	static {
		movies.add(new Movie(1L,"Star Wars: Episode II - Attack of the Clones","Lukas",2002));
	}

	@GET
	public List<Movie> getAllMovies() {
		return movies;
	}

	@GET
	@Path("{movieId}")
	public Movie getMovie(@PathParam("movieId") String movieId) {
		Long id = null;
		try {
			id = Long.valueOf(movieId);
		} catch (NumberFormatException nfe) {
			logger.warn("wrong parameter movieId = " + movieId);
			return new Movie(0L);
		}
		for (Movie movie : movies)
			if (id == movie.getId())
				return movie;

		return new Movie(0L);

	}

	@DELETE
	@Path("{movieId}")
	public Movie deleteMovie(@PathParam("movieId") String movieId) {
		Long id = null;
		try {
			id = Long.valueOf(movieId);
		} catch (NumberFormatException nfe) {
			logger.warn("wrong parameter movieId = " + movieId);
			return new Movie(0L);
		}

		for (Movie movie : movies) {
			if (id == movie.getId()) {
				movies.remove(movie);
				logger.info("movie was deleted " + movie.toString());
				break;
			}
		}
		
		logger.info(movies.size());

		return new Movie(0L);

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void newMovie(Movie movie) throws IOException {
		movie.setId(Math.round(Math.random()));
		movies.add(movie);
		logger.info("add movie " + movie.toString());
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateMovie(Movie movie) throws IOException {

		logger.info("update movie");
		for (Movie m : movies) {
			if (m.getId() == movie.getId()) {
				logger.info("update movie: \nwas " + m.toString() + " \nwill "
						+ movie.toString());
				movies.remove(m);
				movies.add(movie);
				break;
			}
		}

	}

	public static void main(String... args) throws JsonGenerationException,
			JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		String str = mapper.writeValueAsString(new Movie(1L, "Start Wars", "d",
				1996));
		System.out.println(str);
	}

}