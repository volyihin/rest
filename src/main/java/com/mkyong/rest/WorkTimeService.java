package com.mkyong.rest;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.google.common.collect.Lists;

@Path("/workTime")
@Produces(MediaType.APPLICATION_JSON)
public class WorkTimeService {

	private static List<WorkTime> workTimes = Lists.newArrayList();
	private static final Logger logger = Logger.getLogger(MovieService.class);
	private static final Random random = new Random(10000000L);

	static {
		workTimes.add(new WorkTime(DateTime.now(), 0, 1));
	}

	@GET
	public List<WorkTime> getAllWorkTime() {
		return workTimes;
	}

	@GET
	@Path("{userId}")
	public List<WorkTime> getWorkTimeBy(@PathParam("userId") String userId) {
		List<WorkTime> times = Lists.newArrayList();
		Integer id = null;
		try {
			id = Integer.valueOf(userId);
		} catch (NumberFormatException nfe) {
			logger.warn("wrong parameter workTimeId = " + userId);
			return times;
		}
		for (WorkTime workTime : workTimes)
			if (id == workTime.getUserId())
				times.add(workTime);

		return times;

	}

	@DELETE
	@Path("{workTimeId}")
	public void deleteMovie(@PathParam("workTimeId") String workTimeId) {
		Integer id = null;
		try {
			id = Integer.valueOf(workTimeId);
		} catch (NumberFormatException nfe) {
			logger.warn("wrong parameter workTimeId = " + workTimeId);
			return;
		}

		for (WorkTime workTime : workTimes) {
			if (id == workTime.getId()) {
				workTimes.remove(workTime);
				logger.info("workTime was deleted " + workTime.toString());
				break;
			}
		}
		logger.info(workTimes.size());
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void addWorkTime(WorkTime workTime) throws IOException {
		workTime.setId(random.nextInt());
		workTimes.add(workTime);
		logger.info("add workTime " + workTime.toString());
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateWorkTime(WorkTime workTime) throws IOException {

		logger.info("update workTime");
		for (WorkTime w : workTimes) {
			if (w.getId() == workTime.getId()) {
				logger.info("update workTime: \nwas " + w.toString()
						+ " \nwill " + workTime.toString());
				workTimes.remove(w);
				workTimes.add(workTime);
				break;
			}
		}
	}
}
