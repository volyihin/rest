package com.mkyong.rest;

import org.joda.time.DateTime;

public class WorkTime {

	@Override
	public String toString() {
		return "WorkTime [id=" + id + ", time=" + time + ", workType="
				+ workType + ", userId=" + userId + "]";
	}

	private Integer id;
	private DateTime time;
	private Integer workType;
	private Integer userId;
	
	public WorkTime(String userId, String workType, String time) {
		super();
		this.time = new DateTime(Long.valueOf(time));
		this.workType = Integer.valueOf(workType);
		this.userId = Integer.valueOf(userId);;
	}
	
	public WorkTime() {
	}

	public WorkTime(Integer userId, Integer workType, Long time) {
		super();
		this.time = new DateTime(time);
		this.workType = workType;
		this.userId = userId;
	}

	public WorkTime(DateTime time, Integer workType, Integer userId) {
		super();
		this.time = time;
		this.workType = workType;
		this.userId = userId;
	}

	public DateTime getTime() {
		return time;
	}

	public void setTime(DateTime time) {
		this.time = time;
	}

	public Integer getWorkType() {
		return workType;
	}

	public void setWorkType(Integer type) {
		this.workType = type;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
